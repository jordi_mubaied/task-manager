<?php

namespace App\Http\Controllers;

use App\Task;
use Illuminate\Http\Request;

class TaskController extends Controller
{

    public function index()
    {
        $tasks = Task::all();
        return view('base', compact('tasks'));
    }


    public function store(Request $request)
    {
        Task::create($request->all());
        return redirect('/')->with('success', 'Show is successfully saved');
    }

}
