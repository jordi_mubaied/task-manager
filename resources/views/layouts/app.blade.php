<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Task Manager</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- Main css -->
        @section('style')
        <link rel="stylesheet" href="{{ asset('/css/app.css') }}">
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/jq-3.3.1/datatables.min.css"/>
        <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">


        @show
    </head>
    <body>

        <div class="container">
            <div class="row align-items-center justify-content-center full-height flex-md-column flex-lg-row flex-sm-column">
                @section('sidebar')
                    No Menu
                @show
                @section('content')
                    No Content
                @show
            </div>
        </div>


        @section('javascript')
            <!-- jQuery -->
            <script src="//code.jquery.com/jquery.js"></script>
            <!-- DataTables -->
            <script src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
            <!-- Bootstrap JavaScript -->
            <script src="{{ asset('js/app.js') }}" type="text/javascript"></script>
        @show

    </body>
</html>
