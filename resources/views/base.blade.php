@extends('layouts.app')

@section('title', 'Home')

@section('sidebar')
    <div class="col-sm-2 col-md-2 col-lg-4 col-xl-4">
        <nav class="navbar navbar-light navbar-expand-sm px-0 flex-column flex-nowrap">
            <button class="navbar-toggler"
                    type="button"
                    data-toggle="collapse"
                    data-target="#navbarTaskManager"
                    aria-controls="navbarSupportedContent"
                    aria-expanded="false"
                    aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="navbar-collapse collapse" id="navbarTaskManager">
                <div class="nav flex-column">
                    <a class="nav-item nav-link active w-100" href="#">Home</a>
                    <a href="#" class="nav-item nav-link w-100">About</a>
                    <a href="#" class="nav-item nav-link w-100">Contact</a>

                </div>
            </div>
        </nav>
    </div>
@endsection

@section('content')
    <div class="col py-2">
        <h2>Task Manager</h2>

        <form method="post" action="{{ route('store') }}">
            <div class="form-group d-flex">
                @csrf
                <span>
                    <label for="subject">Asunto:</label>
                    <input type="text" class="form-control" name="subject"/>
                </span>
                <span>
                    <label for="priority">Prioridad :</label>
                    <select name="priority" class="form-control" id="priority">
                        @for ($i = 1; $i <= 10; $i++)
                            <option value="{{ $i }}">
                                {{ $i }}
                            </option>
                        @endfor
                    </select>
                </span>
            </div>
            <button type="submit" class="btn btn-primary"><i class="fa fa-calendar-plus-o"></i></button>
        </form>
        <hr>
        <table id="example" class="table table-striped table-bordered">
            <thead>
            <tr>
                <th>Asunto</th>
                <th>Prioridad</th>
                <th>Acciones</th>

            </tr>
            </thead>
            <tbody>
            @foreach($tasks as $task)
            <tr>
                <td>{{ $task->subject }}</td>
                <td>{{ $task->priority }}</td>
                <td class="d-flex">
                    <button class="up btn btn-primary mx-1" data-id="{{ $task->id }}" data-priority="{{ $task->priority }}"><i class="fa fa-arrow-up"></i></button>
                    <button class="down btn btn-primary mx-1" data-id="{{ $task->id }}" data-priority="{{ $task->priority }}"><i class="fa fa-arrow-down"></i> </button>
                    <button class="delete btn btn-primary mx-1" data-id="{{ $task->id }}"><i class="fa fa-trash-o"></i></button>
                </td>

            </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection
