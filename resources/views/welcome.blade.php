<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="htt-ps://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- Main css -->
        <link rel="stylesheet" href="/css/app.css">

    </head>
    <body>

    <script src="js/app.js" type="text/javascript"></script>
    </body>
</html>
