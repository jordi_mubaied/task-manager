
$(document).ready( function () {
    $('#example').DataTable({
        "order": [[ 1, "asc" ]]
    });

    $('.delete').click(function(){
        $.ajax({
            url: 'http://localhost:8000/api/task/'+$(this).attr('data-id'),
            type: 'DELETE',
            success: function ( data ) {
                alert('DELETED');
                location.reload();

            },
            error: function ( errorThrown ) {
                alert('FALLO al Eliminar')
            }
        });

    });

    $('.up').click(function(){
        let actualPriority = parseInt($(this).attr('data-priority'));
        if ( actualPriority > 1 ){
            actualPriority -= 1;
        }
        $.ajax({
            url: 'http://localhost:8000/api/task/'+$(this).attr('data-id'),
            type: 'PUT',
            dataType:'json',
            data:{
                id:$(this).attr('data-id'),
                priority:actualPriority
            },
            success: function ( data ) {
                alert('UP');
                location.reload();

            },
            error: function ( errorThrown ) {
                alert('FALLO al subir')
            }
        });
    });

    $('.down').click(function(){
        let actualPriority = parseInt($(this).attr('data-priority'));
        if ( actualPriority < 10 ){
            actualPriority += 1;
        }
        $.ajax({
            url: 'http://localhost:8000/api/task/'+$(this).attr('data-id'),
            type: 'PUT',
            dataType:'json',
            data:{
                id:$(this).attr('data-id'),
                priority:actualPriority
            },
            success: function ( data ) {
                alert('DOWN');
                location.reload();
            },
            error: function ( errorThrown ) {
                alert('FALLO al bajar')
            }
        });
    });
});
