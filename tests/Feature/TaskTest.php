<?php

namespace Tests\Feature;


use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\RefreshDatabaseState;
use Tests\TestCase;

class TaskTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    use RefreshDatabase;

    public function testCreateTask()
    {
        $response = $this->json('POST', '/api/task', ['subject' => 'hello world','priority' => 1]);

        $response
            ->assertStatus(201)
            ->assertJson([
                'subject' => 'hello world',
                'priority' => 1
            ]);
    }

    public function testListAllTasks()
    {
        $this->json('POST', '/api/task', ['subject' => 'hello world','priority' => 1]);
        $this->json('POST', '/api/task', ['subject' => 'Bad boys II','priority' => 2]);

        $dataExpected[] = [ 'subject' => 'hello world', 'priority' => 1 ];
        $dataExpected[] = [ 'subject' => 'Bad boys II', 'priority' => 2 ];

        $response = $this->get('/api/tasks');
        $response
            ->assertStatus(200)
            ->assertJson( $dataExpected );
    }

    public function testUpdateTask()
    {

        $dataExpected = [ 'subject' => 'mad world', 'priority' => 2 ];

        $this->json('POST', '/api/task', ['subject' => 'hello world','priority' => 1]);
        $response = $this->json('PUT','/api/task/4', $dataExpected);

        $response
            ->assertStatus(200)
            ->assertJson( $dataExpected );

    }

}
